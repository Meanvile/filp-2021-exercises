package exercises04.calculator

import scala.Integral.Implicits.infixIntegralOps

class Calculator[T: Integral] {
  def isZero(t: T): Boolean =
    t == implicitly[Integral[T]].zero

  private def calc(expr: Expr[T]): Option[T] = expr match {
    case Val(x)              => Some(x)
    case Mul(left, right)    => calc(left).flatMap(l => calc(right).map(r => l * r))
    case Div(left, right)    => calc(right).flatMap(r => if (isZero(r)) None else calc(left).map(l => l / r))
    case Plus(left, right)   => calc(left).flatMap(l => calc(right).map(r => l + r))
    case Minus(left, right)  => calc(left).flatMap(l => calc(right).map(r => l - r))
    case If(iff, cond, l, r) => calc(cond).map(iff).flatMap(x => if (x) calc(l) else calc(r))
  }

  def calculate(expr: Expr[T]): Result[T] = calc(expr) match {
    case Some(x) => Success(x)
    case _       => DivisionByZero
  }
}
