package exercises04

sealed trait Tree[+A]
final case class Leaf[A](value: A)                        extends Tree[A]
final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  def fold[A, B](t: Tree[A])(f: A => B)(g: (B, B) => B): B = t match {
    case Leaf(value) =>
      f(value)
    case value: Branch[A] =>
      g(fold(value.left)(f)(g), fold(value.right)(f)(g))
  }

  def size[A](t: Tree[A]): Int = fold(t)(_ => 1)((a, b) => a + b + 1)

  def max(t: Tree[Int]): Int = fold(t)(identity)(math.max)

  def depth[A](t: Tree[A]): Int = fold(t)(_ => 1)((a, b) => math.max(a, b) + 1)

  def map[A, B](t: Tree[A])(f: A => B): Tree[B] =
    fold[A, Tree[B]](t)(t => Leaf(f(t)))((a, b) => Branch(a, b))
}
