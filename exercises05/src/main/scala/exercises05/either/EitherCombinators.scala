package exercises05.either

object EitherCombinators {

  sealed trait Either[+A, +B] {
    def map[BB](f: B => BB): Either[A, BB] = this match {
      case l: Left[A, BB] => l
      case Right(a)       => Right(f(a))
    }

    def flatMap[AA >: A, BB](f: B => Either[AA, BB]): Either[AA, BB] = this match {
      case l: Left[AA, BB] => l
      case Right(a)        => f(a)
    }

    def orElse[EE >: A, C >: B](other: => Either[EE, C]): Either[EE, C] = (this, other) match {
      case (Left(_), Right(_)) => other
      case (Right(_), _)       => this
      case (_, _)              => this
    }

    def map2[AA >: A, BB, C](other: => Either[AA, BB])(f: (B, BB) => C): Either[AA, C] =
      this.flatMap(t => other.map(o => f(t, o)))
  }

  case class Left[+A, +B](get: A) extends Either[A, B]

  case class Right[+A, +B](get: B) extends Either[A, B]

  object Either {
    def fromOption[A, B](option: Option[B])(a: => A): Either[A, B] = option match {
      case Some(x) => Right(x)
      case _       => Left(a)
    }

    def traverse[E, A, B](list: List[A])(f: A => Either[E, B]): Either[E, List[B]] =
      list.foldRight[Either[E, List[B]]](Right(Nil))((x, y) => f(x).map2(y)(_ :: _))

    def sequence[E, A](list: List[Either[E, A]]): Either[E, List[A]] = traverse(list)(identity)
  }

}
