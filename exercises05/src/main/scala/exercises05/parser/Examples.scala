package exercises05.parser

import exercises05.either.EitherCombinators._
import Error._
import scala.util.matching.Regex

object Examples {
  val passportPattern: Regex = new Regex("([0-9]{4}) ([0-9]{6})", "series", "number")

  /**
    * если rawUser.firstName или rawUser.secondName == None, то функция должна вернуть None
    * если rawUser.passport == None или rawUser.thirdName == None, то и в результирующем User эти поля == None
    * passport должен быть передан в формате 1234 567890, если не так, то функция должна вернуть None
    * если rawUser.id не парсится в Long то функция должна вернуть None
    * если rawUser.banned, то вернуть None
    * используйте for-comprehension
    */
  def transformToOption(rawUser: RawUser): Option[User] =
    for {
      _ <- if (rawUser.banned) None
      else Some(true)
      fn <- rawUser.firstName
      sn <- rawUser.secondName
      id <- rawUser.id.toLongOption
      p  <- parsePassport(rawUser.passport)
    } yield User(id, UserName(fn, sn, rawUser.thirdName), p)

  private def validatePassport(passport: Option[String]): Boolean = passport match {
    case Some(x) if passportPattern.matches(x) => true
    case None                                  => true
    case _                                     => false
  }
  private def createPassport(ps: Option[String]): Option[Passport] = ps match {
    case Some(x: String) =>
      passportPattern.findFirstMatchIn(x) match {
        case Some(p) => Some(Passport(p.group("series").toLong, p.group("number").toLong))
      }
    case None => None
  }

  private def parsePassport(ps: Option[String]): Option[Option[Passport]] =
    if (validatePassport(ps)) Some(createPassport(ps))
    else None

  /**
    * если rawUser.firstName или rawUser.secondName == None, то функция должна вернуть Left(InvalidName)
    * если rawUser.passport == None или rawUser.thirdName == None, то и в результирующем User эти поля == None
    * passport должен быть передан в формате 1234 567890, если не так, то функция должна вернуть Left(InvalidPassport)
    * если rawUser.id не парсится в Long то функция должна вернуть Left(InvalidId)
    * если rawUser.banned, то вернуть Left(Banned)
    * у ошибок есть приоритет:
    * 1. Banned
    * 2. InvalidId
    * 3. InvalidName
    * 4. InvalidPassport
    * используйте for-comprehension
    * но для того, чтобы for-comprehension заработал надо реализовать map и flatMap в Either
    */
  def transformToEither(rawUser: RawUser): Either[Error, User] =
    for {
      _ <- if (rawUser.banned) Left(Banned)
      else Right(rawUser.banned)
      id <- Either.fromOption(rawUser.id.toLongOption)(InvalidId)
      fn <- Either.fromOption(rawUser.firstName)(InvalidName)
      sn <- Either.fromOption(rawUser.secondName)(InvalidName)
      p  <- Either.fromOption(parsePassport(rawUser.passport))(InvalidPassport)
    } yield User(id, UserName(fn, sn, rawUser.thirdName), p)
}
