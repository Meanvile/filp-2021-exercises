package exercises07.ex01

import exercises07.data.NonEmptyList
import exercises07.typeclasses._

object Exercise01 {
  object Syntax {
    implicit class SemigroupOps[A](private val a: A) extends AnyVal {
      def |+|(b: A)(implicit semigroup: Semigroup[A]): A =
        semigroup.combine(a, b)
    }

    implicit class PureOps[A](private val a: A) extends AnyVal {
      def pure[F[_]: Applicative]: F[A] =
        Applicative[F].pure(a)
    }

    implicit class FoldableOps[A, F[_]](private val a: F[A]) extends AnyVal {
      def combineAll(implicit monoid: Monoid[A], foldable: Foldable[F]): A =
        foldable.foldLeft(a, monoid.empty)(monoid.combine)

      def foldLeft(b: A)(f: (A, A) => A)(implicit foldable: Foldable[F]): A =
        foldable.foldLeft(a, b)(f)

    }

    implicit class AplicativeOps[A, B, F[_]](private val a: F[A]) extends AnyVal {
      def aproduct(b: F[B])(implicit applicative: Applicative[F]): F[_] = applicative.product(a, b)

      def product(fb: F[B])(implicit ap: Applicative[F]): F[(A, B)] =
        ap.product(a, fb)
    }

    implicit class FunctorOps[F[_], A](private val fa: F[A]) extends AnyVal {
      def map[B](f: A => B)(implicit functor: Functor[F]): F[B] =
        functor.map(fa)(f)
    }

    implicit class TraverseOps[F[_], A](private val fa: F[A]) extends AnyVal {
      def traverse[G[_]: Applicative, B](f: A => G[B])(implicit traverse: Traverse[F]): G[F[B]] =
        traverse.traverse(fa)(f)
    }
  }

  object Instances {
    import Syntax._

    implicit val strMonoid: Monoid[String] = new Monoid[String] {

      override def combine(x: String, y: String): String = x + y

      override def empty: String = ""
    }

    implicit val intMonoid: Monoid[Int] = new Monoid[Int] {
      override def empty: Int = 0

      override def combine(x: Int, y: Int): Int = x + y
    }

    implicit def nonEmptyListSemiGroup[A]: Semigroup[NonEmptyList[A]] =
      (x, y) =>
        NonEmptyList[A](
          x.head,
          x.tail.appended(y.head).appendedAll(y.tail)
        )

    implicit def listMonoid[A]: Monoid[List[A]] = new Monoid[List[A]] {
      override def empty: List[A] = List.empty[A]

      override def combine(x: List[A], y: List[A]): List[A] = x.appendedAll(y)
    }

    implicit val listFoldable: Foldable[List] = new Foldable[List] {
      override def foldLeft[A, B](fa: List[A], b: B)(f: (B, A) => B): B = fa.foldLeft(b)(f)
    }

    implicit val nelFoldable: Foldable[NonEmptyList] = new Foldable[NonEmptyList] {
      override def foldLeft[A, B](fa: NonEmptyList[A], b: B)(f: (B, A) => B): B =
        (List(fa.head) |+| fa.tail).foldLeft(b)(f)
    }

    implicit val listTraverse: Traverse[List] with Applicative[List] = new Traverse[List] with Applicative[List] {
      def traverse[G[_]: Applicative, A, B](fa: List[A])(f: A => G[B]): G[List[B]] =
        fa.foldLeft(List.empty[B].pure[G])((accF, next) =>
          accF.product(f(next)).map { case (acc, next) => acc.appended(next) }
        )

      override def foldLeft[A, B](fa: List[A], b: B)(f: (B, A) => B): B = fa.foldLeft(b)(f)

      override def map[A, B](fa: List[A])(f: A => B): List[B] = fa.map(f)

      override def ap[A, B](ff: List[A => B])(fa: List[A]): List[B] =
        ff.zip(fa).map { case (func, value) => func(value) }

      override def pure[A](x: A): List[A] = List(x)

      override def product[A, B](fa: List[A], fb: List[B]): List[(A, B)] = fa.zip(fb)
    }

    implicit val optionTraverse: Traverse[Option] with Applicative[Option] =
      new Traverse[Option] with Applicative[Option] {
        override def foldLeft[A, B](fa: Option[A], b: B)(f: (B, A) => B): B =
          fa match {
            case Some(value) => f(b, value)
            case _           => b
          }

        override def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa.map(f)

        override def ap[A, B](ff: Option[A => B])(fa: Option[A]): Option[B] = (fa, ff) match {
          case (Some(value), Some(func)) => Some(func(value))
          case _                         => None
        }

        override def pure[A](x: A): Option[A] = Some(x)

        override def product[A, B](fa: Option[A], fb: Option[B]): Option[(A, B)] = (fa, fb) match {
          case (Some(value), Some(value2)) => Some((value, value2))
          case _                           => None
        }

        def traverse[G[_]: Applicative, A, B](fa: Option[A])(f: A => G[B]): G[Option[B]] =
          fa match {
            case Some(value) => f(value).map(Some(_))
            case None        => Option.empty[B].pure[G]
          }
      }

    implicit val nelTraverse: Traverse[NonEmptyList] with Applicative[NonEmptyList] =
      new Traverse[NonEmptyList] with Applicative[NonEmptyList] {
        override def traverse[G[_]: Applicative, A, B](fa: NonEmptyList[A])(f: A => G[B]): G[NonEmptyList[B]] =
          f(fa.head).aproduct(fa.tail.traverse(f)).map {
            case (head: B, tail: List[B]) => NonEmptyList(head, tail)
          }

        override def foldLeft[A, B](fa: NonEmptyList[A], b: B)(f: (B, A) => B): B = nelFoldable.foldLeft(fa, b)(f)

        override def map[A, B](fa: NonEmptyList[A])(f: A => B): NonEmptyList[B] =
          new NonEmptyList[B](f(fa.head), fa.tail.map(f))

        override def ap[A, B](ff: NonEmptyList[A => B])(fa: NonEmptyList[A]): NonEmptyList[B] =
          new NonEmptyList[B](ff.head(fa.head), ff.tail.zip(fa.tail).map { case (func, value) => func(value) })

        override def pure[A](x: A): NonEmptyList[A] = NonEmptyList[A](x)

        override def product[A, B](fa: NonEmptyList[A], fb: NonEmptyList[B]): NonEmptyList[(A, B)] =
          new NonEmptyList((fa.head, fb.head), fa.tail.zip(fb.tail))
      }
  }
}
