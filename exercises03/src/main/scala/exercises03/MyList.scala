package exercises03

import scala.annotation.tailrec

sealed trait MyList[+A]

final case class Cons[A](head: A, tail: MyList[A]) extends MyList[A]

case object Nil extends MyList[Nothing]

object MyList {
  def sum(list: MyList[Int]): Int = tailrecSum(list, 0)

  @tailrec
  def tailrecSum(list: MyList[Int], sum: Int): Int = list match {
    case Cons(h, t) => tailrecSum(t, h + sum)
    case Nil        => sum
  }

  def reverse[A](list: MyList[A]): MyList[A] = tailrecReverse(list, Nil)

  @tailrec
  def tailrecReverse[A](list: MyList[A], reversed: MyList[A]): MyList[A] = list match {
    case Nil        => reversed
    case Cons(h, t) => tailrecReverse(t, Cons(h, reversed))
  }
}
