package exercises03.game

object Game {
  def parseState(input: String, number: Int): State = input.toIntOption match {
    case Some(n) if n > number                   => NumberIsSmaller
    case Some(n) if n < number                   => NumberIsBigger
    case Some(n) if n == number                  => Guessed
    case None if input == GameController.IGiveUp => GiveUp
    case None                                    => WrongInput
  }

  def action(state: State, number: Int): GameController => Unit = state match {
    case NumberIsSmaller => _.numberIsSmaller()
    case NumberIsBigger  => _.numberIsBigger()
    case Guessed         => _.guessed()
    case GiveUp          => _.giveUp(number)
    case WrongInput      => _.wrongInput()
  }

  def completed(state: State): Boolean = state match {
    case Guessed => true
    case GiveUp  => true
    case _       => false
  }
}
