package exercises03

object Partial {
  def combo[I, T](funcs: List[PartialFunction[I, T]]): I => Option[T] = {
    funcs.foldLeft(PartialFunction.empty[I, T])((a, b) => a.orElse(b)).lift
  }
}
