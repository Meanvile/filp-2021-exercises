package exercises03

object SetFunctions {
  type Set[A] = A => Boolean

  def contains[A](s: Set[A], elem: A): Boolean = s(elem)

  def singletonSet[A](elem: A): Set[A] = elem == _

  def union[A](s: Set[A], t: Set[A]): Set[A] = x => contains(s, x) || contains(t, x)

  def intersect[A](s: Set[A], t: Set[A]): Set[A] = x => contains(s, x) && contains(t, x)

  def diff[A](s: Set[A], t: Set[A]): Set[A] = x => contains(s, x) && !contains(t, x)

  def symmetricDiff[A](s: Set[A], t: Set[A]): Set[A] = diff(union(s, t), intersect(t, s))

  def filter[A](s: Set[A], p: A => Boolean): Set[A] = x => contains(s, x) && p(x)

  def cartesianProduct[A, B](as: Set[A], bs: Set[B]): Set[(A, B)] = {
    case (a, b) => contains(as, a) && contains(bs, b)
  }
}
