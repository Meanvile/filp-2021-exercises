package examples

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}

object Functor {
  @inline
  def apply[F[_]](implicit inst: Functor[F]): Functor[F] = inst
}

object Implicits {

  implicit val listFunctor: Functor[List] = new Functor[List] {
    override def map[A, B](fa: List[A])(f: A => B): List[B] =
      fa.map(f)
  }

  implicit val setFunctor: Functor[Set] = new Functor[Set] {
    override def map[A, B](fa: Set[A])(f: A => B): Set[B] =
      fa.map(f)
  }

  type Id[A] = A

  implicit val idFunctor: Functor[Id] = new Functor[Id] {
    override def map[A, B](fa: Id[A])(f: A => B): Id[B] =
      f(fa)
  }
}

object Examples extends App {
  def double[F[_]: Functor](fa: F[Int]): F[Int] = Functor[F].map(fa)(_ * 2)

  import Implicits._

  println(double(List(1, 2, 3)))
  println(double(Set(1, 2, 3)))
  println(double[Id](1))

}
