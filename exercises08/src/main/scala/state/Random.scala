package state

import typeclasses.Monad

import scala.annotation.tailrec

object StatefulRandom {

  import scala.util.Random

  // Если вам необходимо сгенерить рандомное число, то вы можете использовать класс scala.util.Random.
  println(Random.nextDouble)
  println(Random.nextInt)
  println(Random.nextInt)

  // Каждый вызов возвращает новое число. Можно предположить, что rnd имеет какое-то внутреннее
  // состояние. Когда происходит очередной вызов nextInt/nextDouble, то rnd не только возвращает
  // новое случайное число, но изменяет внутреннее состояние.
  //
  // Такое скрытое изменение внутреннего состояние имеет ряд недостатков: программы тяжелее
  // композировать, тестировать, усложняется написание многопоточных программ.
}

object StatelessRandom {

  // Реализуйте функцию, которая будет генерировать пару случайных целых чисел
  def pair(rnd: Random): ((Int, Int), Random) = rnd.nextInt match {
    case (v1: Int, random: Random) =>
      random.nextInt match {
        case (v2: Int, random2: Random) => ((v1, v2), random2)
      }
  }

  // Функцию, которая генерирует случайное число от нуля включительно до единицы невключительно
  def double(rnd: Random): (Double, Random) = nonNegativeInt(rnd) match {
    case (v: Int, random: Random) => (v / Int.MaxValue.toDouble, random)
  }

  // и простой пример использования
  // Вместо скрытого изменения состояния, мы теперь делаем все изменения явными. Чтобы получать
  // каждый раз разные значения, мы вызываем nextInt на разных объектах: rnd, nextRnd1, nextRnd2 и
  // так далее.

  // Функцию, которая генерирует неотрицальные числа
  def nonNegativeInt(rnd: Random): (Int, Random) = rnd.nextInt match {
    case (v: Int, random: Random) => (Math.abs(v), random)
  }

  // Чтобы избавиться от скрытого изменяемого состояния, давайте сделаем его явным. Новая функция
  // nextInt возвращает не только случайное число, но и новое состояние генератора случайных чисел.
  trait Random {
    def nextInt: (Int, Random)
  }

  // В качесте простой реализации генератора случайных чисел будем использовать следующий класс
  case class SimpleRandom(seed: Long) extends Random {
    override def nextInt: (Int, Random) = {
      val newSeed = 13 * seed + 41
      val int     = (newSeed >>> 3).toInt
      (int, SimpleRandom(newSeed))
    }
  }
}

// Нам удалось избавиться от скрытого изменяемого состояния, но приходится передавать теперь его явно.
// Это приводит к написанию достаточно однообразного кода. Давайте попробуем немного модифицировать
// подход, чтобы избавиться от этого.
object BetterStatelessRandom {

  import typeclasses.Monad.syntax._
  import StatelessRandom.Random

  // Функция возвращает случайное целое число
  val nextInt: RandomState[Int] = RandomState(x => x.nextInt)
  // Функция возвращает случайное неотрицальное целое число
  val nonNegativeInt: RandomState[Int] =
    for {
      x <- nextInt
    } yield Math.abs(x)

  // Теперь класс RandomState может быть использован внутри for comprehension
  // Функция возвращает пару случайных неотрицальных целых чисел
  val pair: RandomState[(Int, Int)] =
    for {
      y <- nonNegativeInt
      z <- nonNegativeInt
    } yield (y, z)
  // Функция возвращает случайное число от нуля до единицы
  val double: RandomState[Double] =
    for {
      x <- nonNegativeInt
    } yield x / Int.MaxValue.toDouble
  // Функция возвращает список случайной длины из случайных целых чисел
  val randomList: RandomState[List[Int]] =
    for {
      n <- nonNegativeInt
      q <- sequence[Int](Range(0, n).toList.map(_ => nextInt)) //List[RandomState[Int]
    } yield q

  // Функция должна сконвертировать список из случайных состояний в случайное состояние, которое
  // возвращает список.
  def sequence[A](xs: List[RandomState[A]]): RandomState[List[A]] =
    xs.foldLeft(List.empty[A].pure[RandomState])((x, y) =>
      for {
        list <- x
        item <- y
      } yield list.prepended(item)
    )

  // Можно заметить, что наши функции pair, nonNegativeInt, double имеют одинаковый шаблон
  // Rnd => (A, Rnd), где тип A зависит от конкретной функции. Можно сказать, что эти функции
  // описывают переход состояния. Давайте обобщим этот шаблон в виде класса RandomState.
  case class RandomState[A](run: Random => (A, Random))

  object RandomState {

    implicit val monad: Monad[RandomState] = new Monad[RandomState] {
      override def pure[A](a: A): RandomState[A] = RandomState[A](x => (a, x))
      override def map[A, B](fa: RandomState[A])(f: A => B): RandomState[B] =
        RandomState(x =>
          fa.run(x) match {
            case (a: A, random: Random) => (f(a), random)
          }
        )

      override def flatMap[A, B](fa: RandomState[A])(f: A => RandomState[B]): RandomState[B] =
        RandomState(x =>
          fa.run(x) match {
            case (a: A, random: Random) => f(a).run(random)
          }
        )
    }
  }
}
