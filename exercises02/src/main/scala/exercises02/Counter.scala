package exercises02

import scala.util.matching.Regex

object Counter {
  val allWordPattern: Regex     = "[^\\s.,!?:\\n\\t\\r()]+".r
  val englishWordPattern: Regex = "[^\\s.,!?:\\n\\t\\r()А-Яа-я0-9]+".r
  val numberPattern: Regex      = "([0-9]+[.,]+[0-9]+)|[0-9]+".r

  /**
    * Посчитать количество вхождений слов в тексте
    * слово отделено символами [\s.,!?:\n\t\r]
    */
  def countWords(text: String): Map[String, Int] = {
    find(text, allWordPattern)
  }

  /**
    * Посчитать количество вхождений английских слов в тексте
    * слово отделено символами [\s.,!?:\n\t\r]
    */
  def countEnglishWords(text: String): Map[String, Int] = {
    find(text, englishWordPattern)
  }

  /**
    * Посчитать количество вхождений чисел в тексте
    * число отделено символами [\s!?:\n\t\r]
    */
  def countNumbers(text: String): Map[String, Int] = {
    find(text, numberPattern)
  }

  def find(text: String, wordPattern: Regex): Map[String, Int] = {
    val words = wordPattern.findAllIn(text.toLowerCase()).toArray
    words.groupMapReduce(identity)(_ => 1)(_ + _)
  }
}
