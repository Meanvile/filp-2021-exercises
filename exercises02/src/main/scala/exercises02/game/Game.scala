package exercises02.game

import scala.annotation.tailrec

class Game(controller: GameController) {
  @tailrec
  final def play(number: Int): Unit = {
    controller.askNumber()
    val input = controller.nextLine()
    if (input == GameController.IGiveUp) {
      controller.giveUp(number)
    } else
      input.toIntOption match {
        case Some(playerNumber: Int) =>
          if (playerNumber == number)
            controller.guessed()
          else if (playerNumber > number) {
            controller.numberIsSmaller()
            play(number)
          } else if (playerNumber < number) {
            controller.numberIsBigger()
            play(number)
          }
        case _ =>
          controller.wrongInput()
          play(number)
      }
  }
}
