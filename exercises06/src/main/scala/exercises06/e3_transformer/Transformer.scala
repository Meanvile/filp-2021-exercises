package exercises06.e3_transformer

import exercises06.e3_transformer.Error.{InvalidId, InvalidName}

trait Transformer[A, B] {
  def toOption(a: A): Option[B]

  def toEither(a: A): Either[Error, B]
}

object TransformerInstances {
  implicit val transformer: Transformer[RawUser, User] = new Transformer[RawUser, User] {
    override def toOption(ru: RawUser): Option[User] =
      for {
        fn <- ru.firstName
        sn <- ru.secondName
        id <- ru.id.toLongOption
      } yield User(id, UserName(fn, sn, ru.thirdName))

    override def toEither(ru: RawUser): Either[Error, User] =
      for {
        fn <- ru.firstName.toRight(InvalidName)
        sn <- ru.secondName.toRight(InvalidName)
        id <- ru.id.toLongOption.toRight(InvalidId)
      } yield User(id, UserName(fn, sn, ru.thirdName))
  }
}

object TransformerSyntax {
  implicit class TransformerOps[A](private val a: A) extends AnyVal {
    def transformToOption[B](implicit inst: Transformer[A, B]): Option[B] = inst.toOption(a)

    def transformToEither[B](implicit inst: Transformer[A, B]): Either[Error, B] = inst.toEither(a)
  }

}

object Examples {
  import TransformerInstances._
  import TransformerSyntax._

  RawUser("1234", Some(""), Some(""), None).transformToOption[User]
  RawUser("1234", Some(""), Some(""), None).transformToEither[User]
}
