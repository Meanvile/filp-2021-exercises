package exercises06.e4_eq

trait Eq[A] {
  def eqv(a: A, b: A): Boolean
}

object Eq {
  def apply[A](implicit eq: Eq[A]): Eq[A] = eq
}

object EqInstances {
  import EqSyntax.EqOps

  implicit val intEq: Eq[Int] = (a, b) => a == b

  implicit val boolEq: Eq[Boolean] = (a, b) => a == b

  implicit def listEq[A](implicit e: Eq[A]): Eq[List[A]] = (a, b) => a.corresponds(b)((x, y) => x === y)

  implicit def optEq[A](implicit e: Eq[A]): Eq[Option[A]] = (a, b) => a.corresponds(b)((x, y) => x === y)
}

object EqSyntax {
  implicit class EqOps[A](private val a: A) extends AnyVal {
    def eqv(b: A)(implicit ev: Eq[A]): Boolean = ev.eqv(a, b)
    def ===(b: A)(implicit ev: Eq[A]): Boolean = eqv(b)
    def !==(b: A)(implicit ev: Eq[A]): Boolean = !eqv(b)
  }
}

object Examples {
  import EqInstances._
  import EqSyntax._

  1 eqv 1 // возвращает true
  1 === 2 // возвращает false
  1 !== 2 // возвращает true
//   1 === "some-string" // не компилируется
//   1 !== Some(2) // не компилируется
  List(true) === List(true) // возвращает true
}
