package competition

import cats.Monad
import cats.syntax.all._
import service.{TwitterService, domain}
import twitter.domain._

import scala.math.Ordered.orderingToOrdered

class CompetitionMethods[F[_]: Monad](service: TwitterService[F]) {

  def unlikeAll(user: User, tweetIds: List[TweetId]): F[Unit] =
    for {
      t <- service.getTweets(tweetIds)
      v <- t.found.toList.traverse(x => service.unlike(user, x.id))
    } yield v

  def topAuthor(tweetIds: List[TweetId]): F[Option[User]] =
    for {
      tweets <- service.getTweets(tweetIds)
    } yield tweets.found
      .maxByOption(identity)((x, y) => (x.likedBy.size, y.created).compareTo(y.likedBy.size, x.created))
      .map(x => x.author)
}
