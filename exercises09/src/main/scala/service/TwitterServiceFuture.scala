package service

import service.domain.GetTweetResponse._
import service.domain.{GetTweetResponse, GetTweetsResponse}
import twitter.TwitterApi
import twitter.domain.TwitterError.{LikeAlreadyExistError, LikeNotExistError, TweetNotExistError}
import twitter.domain.{TweetId, TweetInfo, User}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Success, Try}

class TwitterServiceFuture(api: TwitterApi)(implicit ec: ExecutionContext) extends TwitterService[Future] {

  def tweet(user: User, text: String): Future[TweetId] =
    future[TweetId, TweetId](api.tweet(user, text), identity)

  def future[A, B](f: (Try[A] => Unit) => Unit, g: Try[A] => Try[B]): Future[B] = {
    val promise = Promise[B]
    Future(f(x => promise.complete(g(x))))
    promise.future
  }

  def like(user: User, tweetId: TweetId): Future[Unit] =
    future[Unit, Unit](api.like(user, tweetId), identity).recover { case LikeAlreadyExistError => () }

  def unlike(user: User, tweetId: TweetId): Future[Unit] =
    future[Unit, Unit](api.unlike(user, tweetId), identity).recover { case LikeAlreadyExistError => () }.recover {
      case LikeNotExistError => ()
    }

  def getTweets(ids: List[TweetId]): Future[GetTweetsResponse] = {
    Future
      .traverse(ids)(getTweet)
      .map(_.foldLeft(GetTweetsResponse(Set.empty[TweetId], Set.empty[TweetInfo])) { (acc, next) =>
        next match {
          case GetTweetResponse.Found(info)       => GetTweetsResponse(acc.notFound, acc.found + info)
          case GetTweetResponse.NotFound(tweetId) => GetTweetsResponse(acc.notFound + tweetId, acc.found)
        }
      })
  }

  def getTweet(tweetId: TweetId): Future[GetTweetResponse] =
    future[TweetInfo, GetTweetResponse](api.get(tweetId), {
      case Success(value) => Success(Found(value))
      case _              => Success(NotFound(tweetId))
    })
}
