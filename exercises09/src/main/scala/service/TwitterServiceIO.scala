package service

import cats.effect.IO
import cats.effect.IO.asyncF
import cats.syntax.all._
import service.domain.GetTweetResponse.{Found, NotFound}
import service.domain._
import twitter.domain.TwitterError._
import twitter.TwitterApi
import twitter.domain._

import scala.util.{Success, Try}

// Воспользуйтесь синтаксисом map, recover, traverse из cats.syntax.all_
class TwitterServiceIO(api: TwitterApi) extends TwitterService[IO] {

  def tweet(user: User, text: String): IO[TweetId] =
    callbacker[TweetId, TweetId](api.tweet(user, text), identity)(x => x.toEither)

  def like(user: User, tweetId: TweetId): IO[Unit] =
    callbacker[Unit, Unit](api.like(user, tweetId), identity)(x =>
      x.recover { case LikeAlreadyExistError => () }.toEither
    )

  def unlike(user: User, tweetId: TweetId): IO[Unit] =
    callbacker[Unit, Unit](api.unlike(user, tweetId), identity)(x => x.recover { case LikeNotExistError => () }.toEither
    )

  def callbacker[A, B](a: (Try[A] => Unit) => Unit, conv: Try[A] => Try[B])(
      f: Try[B] => Either[Throwable, B]
  ): IO[B] = {
    IO.async(callback => a(x => callback(f(conv(x)))))
  }

  def getTweets(ids: List[TweetId]): IO[GetTweetsResponse] =
    ids
      .traverse(getTweet)
      .map(_.foldLeft(GetTweetsResponse(Set.empty[TweetId], Set.empty[TweetInfo])) { (acc, next) =>
        next match {
          case GetTweetResponse.Found(info)       => GetTweetsResponse(acc.notFound, acc.found + info)
          case GetTweetResponse.NotFound(tweetId) => GetTweetsResponse(acc.notFound + tweetId, acc.found)
        }
      })

  def getTweet(tweetId: TweetId): IO[GetTweetResponse] =
    callbacker[TweetInfo, GetTweetResponse](api.get(tweetId), {
      case Success(value) => Success(Found(value))
      case _              => Success(NotFound(tweetId))
    })(x => x.toEither)
}
